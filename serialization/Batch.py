import os
import cPickle
import numpy as np


class MetaBatch:

    def __init__(self, num_cases_per_batch, label_names, num_vis, data_mean):
        self.batch_data = \
            {
                'num_cases_per_batch': num_cases_per_batch,
                'label_names': label_names,
                'num_vis': num_vis,
                'data_mean': data_mean
            }

        self.batch_name = 'batches.meta'

    def write(self, saving_folder):
        with open(os.path.join(saving_folder, self.batch_name), 'wb') as writed_file:
            cPickle.dump(self.batch_data, writed_file)


class Batch:

    def __init__(self, batch_label):
        self.batch_data = {'batch_label': batch_label, 'labels': [], 'data': [], 'filenames': []}

    def append(self, label, data, filename):
        self.batch_data['labels'] += [label]
        self.batch_data['data'] += [data]
        self.batch_data['filenames'] += [filename]

    def write(self, name, saving_folder):
        self.batch_data['data'] = np.rot90(np.flipud(np.array(self.batch_data['data'])), k=3)
        with open(os.path.join(saving_folder, name), 'wb') as writed_file:
            cPickle.dump(self.batch_data, writed_file)
