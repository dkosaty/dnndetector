import os
import re
from PIL import Image
import numpy as np


class Sample:

    def __init__(self, dataset_path, dataset_type, scope, num_cases_per_batch):
        self.dataset_path = dataset_path
        self.type = dataset_type
        self.batches_names = Sample.parse_range(scope)
        self.size = len(self.batches_names)*num_cases_per_batch

    @staticmethod
    def parse_range(scope):
        pattern = r'(\d+)-?(\d+)?'

        founded = re.match(pattern, scope)

        if founded.group(2) is None:
            first = int(founded.group(1))
            last = first
        else:
            first, last = int(founded.group(1)), int(founded.group(2))

        batches_names = ['data_batch_'+str(num) for num in range(first, last+1)]

        return batches_names

    def read(self):
        def get_data(images_paths):
            data = {}
            for line in images_paths:
                image = Image.open(os.path.join(self.dataset_path, line[:len(line)-1]))
                resized_image = image.resize((32, 32))
                image_data = np.fliplr(np.rot90(np.array(resized_image, order='C'), k=3))
                data[line] = image_data
            return data

        positive_file = open(os.path.join(self.dataset_path, self.type, 'pos.lst'), 'r')
        negative_file = open(os.path.join(self.dataset_path, self.type, 'neg.lst'), 'r')

        try:
            return get_data(positive_file.readlines()), get_data(negative_file.readlines())
        except IOError as error:
            print 'IOError excepted: ', error.strerror
        finally:
            negative_file.close()
            positive_file.close()
