# script paramentrs:
# -c *-serialize.cfg

import os
from ConfigParser import SafeConfigParser

from optparse import OptionParser

from Serializer import Serializer
from Sample import Sample


def main():
    options = get_options()

    dataset_path = options['dataset-path']
    train_range = options['train-range']
    test_range = options['test-range']
    num_cases_per_batch = options['num-cases-per-batch']
    saving_folder = options['saving-folder']

    if not os.path.exists(saving_folder):
        os.makedirs(saving_folder)

    serializer = Serializer(saving_folder)

    train_sample = Sample(dataset_path, 'Train', train_range, num_cases_per_batch)

    test_sample = Sample(dataset_path, 'Test', test_range, num_cases_per_batch)

    print 'serialize the batches meta'
    serializer.create_meta_batch(num_cases_per_batch, train_sample, test_sample)

    print 'serialize the training instances'
    serializer.serialize(train_sample)

    print 'serialize the test instances'
    serializer.serialize(test_sample)


def get_options():
    option_parser = OptionParser()

    option_parser.add_option('-c', '--configuration_file', dest='config', type='string')
    (options, args) = option_parser.parse_args()

    config_parser = SafeConfigParser()
    config_parser.read(options.config)

    options = dict()
    options['dataset-path'] = config_parser.get('data', 'dataset-path')
    options['train-range'] = config_parser.get('ranges', 'train-range')
    options['test-range'] = config_parser.get('ranges', 'test-range')
    options['num-cases-per-batch'] = config_parser.getint('serialize', 'num-cases-per-batch')
    options['saving-folder'] = config_parser.get('serialize', 'saving-folder')

    return options

if __name__ == '__main__':
    main()
