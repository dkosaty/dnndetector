import os
import re
import numpy as np
from PIL import Image, ImageOps

from Batch import MetaBatch, Batch


class Serializer:

    def __init__(self, saving_folder):
        self.saving_folder = saving_folder

        self.image_size = 32

        # self.positive_label = [0]*16 + [1]
        self.positive_label = 1
        # self.negative_label = [0]*17
        self.negative_label = 0

    def create_meta_batch(self, num_cases_per_batch, train_sample, test_sample):
        def func(image_name):
            image = Image.open(image_name)
            img_data = ImageOps.fit(image, (self.image_size, self.image_size))
            img_data = np.array(img_data)
            img_data = img_data.T.reshape(3, -1).reshape(-1)
            return img_data.astype(np.single)

        names_and_labels = []

        def get_names_and_labels_list(sample):
            positive_data, negative_data = sample.read()
            to = sample.size/2
            for line in positive_data.keys()[:to]:
                names_and_labels.append((os.path.join(sample.dataset_path, line[:len(line)-1]), self.positive_label))
            for line in negative_data.keys()[:to]:
                names_and_labels.append((os.path.join(sample.dataset_path, line[:len(line)-1]), self.negative_label))

        get_names_and_labels_list(train_sample)
        get_names_and_labels_list(test_sample)
        rows = [func(name) for name, label in names_and_labels]
        data = np.vstack([r for r in rows if r is not None])

        # label_names = ['a'+str(i)+str(j) for i in range(1,5) for j in range(1,5)] + ['A']
        label_names = ['person', 'non_person']
        meta_batch = MetaBatch(num_cases_per_batch, label_names, 3*32*32, data.mean(axis=0))

        meta_batch.write(self.saving_folder)

    def serialize(self, sample):
        def parse_image_name(input_line):
            founded = re.search(r'\w*\.+\w+', input_line)
            return founded.group()

        positive_data, negative_data = sample.read()

        batches_number = len(sample.batches_names)
        for num in range(batches_number):
            start = num*sample.size/(2*len(sample.batches_names))
            stop = (num+1)*sample.size/(2*len(sample.batches_names))

            batch_naming = sample.type[0].lower() + sample.type[1:] + 'ing '
            batch_label = batch_naming + 'batch ' + str(num+1) + ' of ' + str(batches_number)
            batch = Batch(batch_label)

            for line in positive_data.keys()[start:stop]:
                data = positive_data.get(line)
                batch.append(self.positive_label, data.T.flatten('C'), parse_image_name(line))

            for line in negative_data.keys()[start:stop]:
                data = negative_data.get(line)
                batch.append(self.negative_label, data.T.flatten('C'), parse_image_name(line))

            batch.write(sample.batches_names[num], self.saving_folder)
