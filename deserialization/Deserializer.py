import os
import re
import numpy as np
from deserialize import unpickle
from scipy import misc


class Deserializer:

    def __init__(self, options):
        self.batches_path = options['batches-path']
        self.saving_folder = options['saving-folder']

        train_batches_names = Deserializer.parse_range(options['train-range'])
        test_batches_names = Deserializer.parse_range(options['test-range'])
        self.names_batches_list = {'Train': train_batches_names, 'Test': test_batches_names}

    @staticmethod
    def parse_range(input_range):
        pattern = r'(\d+)-?(\d+)?'

        founded = re.match(pattern, input_range)

        if founded.group(2) is None:
            first = int(founded.group(1))
            last = first
        else:
            first, last = int(founded.group(1)), int(founded.group(2))

        batches_names = ['data_batch_'+str(num) for num in range(first, last+1)]

        return batches_names

    def deserialize(self, dataset_type):
        for filename in self.names_batches_list[dataset_type]:
            raw_data = unpickle(os.path.join(self.batches_path, filename))

            for i in range(len(raw_data['data'][1])):
                image = Deserializer.extract_image(raw_data['data'][:, i])

                label = raw_data['labels'][i]
                label_name = 'person' if label == 1 else 'non_person'

                # misc.imshow(image)

                misc.imsave(os.path.join(self.saving_folder, dataset_type, label_name, raw_data['filenames'][i]), image)

    @staticmethod
    def extract_image(data):
        data_format = np.zeros((32, 32, 3), np.uint8)

        for r in range(32):
            for c in range(32):
                k = r+32*c
                data_format[r, c] = [data[k], data[k+1024], data[k+2048]]

        return data_format.T
