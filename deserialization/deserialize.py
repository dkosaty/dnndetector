# script parameters:
# -c *-deserialize.cfg

from optparse import OptionParser
from ConfigParser import SafeConfigParser
from Deserializer import *


def main():
    options = get_options()

    if not os.path.exists(options['saving-folder']):
        os.makedirs(options['saving-folder'])

    meta = unpickle(os.path.join(options['batches-path'], 'batches.meta'))

    create_directory(os.path.join(options['saving-folder'], 'Train'), meta=meta)
    create_directory(os.path.join(options['saving-folder'], 'Test'), meta=meta)

    deserializer = Deserializer(options)

    print 'parse the train instances'
    deserializer.deserialize('Train')

    print 'parse the test instances'
    deserializer.deserialize('Test')


def get_options():
    option_parser = OptionParser()
    option_parser.add_option('-c', '--configuration_file', dest='config', type='string')
    (options, args) = option_parser.parse_args()

    config_parser = SafeConfigParser()
    config_parser.read(options.config)

    options = dict()
    options['batches-path'] = config_parser.get('batches', 'batches-path')
    options['train-range'] = config_parser.get('ranges', 'train-range')
    options['test-range'] = config_parser.get('ranges', 'test-range')
    options['saving-folder'] = config_parser.get('deserialize', 'saving-folder')

    return options


def unpickle(input_file):
    import cPickle
    fo = open(input_file, 'rb')
    data_dict = cPickle.load(fo)
    fo.close()
    return data_dict


def create_directory(directory_name, meta):
    if not os.path.exists(directory_name):
        os.makedirs(directory_name)

    for label_name in meta['label_names']:
        folder_name = os.path.join(directory_name, label_name)
        if not os.path.exists(folder_name):
            os.makedirs(folder_name)

if __name__ == '__main__':
    main()
