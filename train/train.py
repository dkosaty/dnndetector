# environment variables:
# LD_LIBRARY_PATH = /usr/local/cuda-7.5/lib64
# PYTHONPATH = /home/dmitry/cuda_convnet
#
# script parameters:
# -c dnn-params/*-dnn.cfg

from optparse import OptionParser
from DNNDetector import DNNDetector


def main():
    option_parser = OptionParser()

    option_parser.add_option('-c', '--configuration_file', dest='config_file', type='string')

    (options, args) = option_parser.parse_args()

    cnn_detector = DNNDetector(options.config_file)

    cnn_detector.train()

if __name__ == '__main__':
    main()
