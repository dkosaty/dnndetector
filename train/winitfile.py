import os

from convnet import ConvNet, IGPUModel
from options import *


class Initializer:

    def __init__(self):
        self.op = OptionsParser()
        self.op.add_option("f", "load_file", StringOptionParser, "Load file", default="", excuses=OptionsParser.EXCLUDE_ALL)

        save_path = '/home/dmitry/PycharmProjects/DNNDetector/train/results/cifar-save-folder'

        self.load_dic = self.load_checkpoint(save_path)

    def load_checkpoint(self, save_path):
        load_dic = None

        absolute_path = os.path.abspath(save_path)
        checkpoint_path = Initializer.get_checkpoint_path(absolute_path)

        if checkpoint_path:
            self.op.options['load_file'].set_value(checkpoint_path)

            load_dic = IGPUModel.load_checkpoint(self.op.options['load_file'].value)
            old_op = load_dic['op']
            old_op.merge_from(self.op)
            self.op = old_op

        return load_dic


    @staticmethod
    def get_checkpoint_path(save_path):
        checkpoint_path = None

        if os.path.exists(save_path):
            convnet_folder = str(filter(lambda x: x.startswith('ConvNet'), os.listdir(save_path)).pop())
            if convnet_folder:
                convnet_path = os.path.join(save_path, convnet_folder)
                if os.path.exists(convnet_path):
                    checkpoint_name = str(os.listdir(convnet_path).pop())
                    if checkpoint_name:
                        checkpoint_path = os.path.join(save_path, convnet_folder, checkpoint_name)

        return checkpoint_path


initializer = Initializer()
load_dic = initializer.load_dic


def load_conv1_weights(name, idx, shape, params=None):
    return load_dic['model_state']['layers'][2]['weights'][0]


def load_conv1_biases(name, shape, params=None):
    return load_dic['model_state']['layers'][2]['biases']


def load_conv2_weights(name, idx, shape, params=None):
    return load_dic['model_state']['layers'][5]['weights'][0]


def load_conv2_biases(name, shape, params=None):
    return load_dic['model_state']['layers'][5]['biases']


def load_conv3_weights(name, idx, shape, params=None):
    return load_dic['model_state']['layers'][8]['weights'][0]


def load_conv3_biases(name, shape, params=None):
    return load_dic['model_state']['layers'][8]['biases']

