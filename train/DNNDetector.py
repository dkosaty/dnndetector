import os

from ConfigParser import SafeConfigParser

from convnet import ConvNet, IGPUModel
from options import *


class DNNDetector:

    def __init__(self, config_file):
        config_parser = SafeConfigParser()

        # parse the configuration file
        config_parser.read(config_file)

        self.op = ConvNet.get_options_parser()

        save_path = config_parser.get('train', 'save-path')
        self.load_dic = self.load_checkpoint(save_path)

        self.set_options(config_parser)

    def load_checkpoint(self, save_path):
        load_dic = None

        absolute_path = os.path.abspath(save_path)
        checkpoint_path = DNNDetector.get_checkpoint_path(absolute_path)

        if checkpoint_path:
            self.op.options['load_file'].set_value(checkpoint_path)

            load_dic = IGPUModel.load_checkpoint(self.op.options['load_file'].value)
            old_op = load_dic['op']
            old_op.merge_from(self.op)
            self.op = old_op

        return load_dic

    @staticmethod
    def get_checkpoint_path(save_path):
        checkpoint_path = None

        if os.path.exists(save_path):
            convnet_folder = str(filter(lambda x: x.startswith('ConvNet'), os.listdir(save_path)).pop())
            if convnet_folder:
                convnet_path = os.path.join(save_path, convnet_folder)
                if os.path.exists(convnet_path):
                    checkpoint_name = str(os.listdir(convnet_path).pop())
                    if checkpoint_name:
                        checkpoint_path = os.path.join(save_path, convnet_folder, checkpoint_name)

        return checkpoint_path

    def set_options(self, config_parser):
        self.op.options['data_path'].set_value(config_parser.get('data', 'data-path'))
        self.op.options['dp_type'].set_value(config_parser.get('data', 'data-provider'))
        self.op.options['layer_def'].set_value(config_parser.get('topology', 'layer-def'))
        self.op.options['layer_params'].set_value(config_parser.get('topology', 'layer-params'))
        self.op.options['train_batch_range'].set_value(config_parser.get('ranges', 'train-range'))
        self.op.options['test_batch_range'].set_value(config_parser.get('ranges', 'test-range'))
        self.op.options['save_path'].set_value(config_parser.get('train', 'save-path'))
        self.op.options['testing_freq'].set_value(config_parser.get('train', 'test-freq'))
        self.op.options['num_epochs'].set_value(config_parser.get('train', 'epochs'))
        self.op.options['gpu'].set_value('-1')
        self.op.options['conv_to_local'].set_value('[]')
        self.op.options['unshare_weights'].set_value('')
        self.op.options['minibatch_size'].set_value('128')
        self.op.options['max_filesize_mb'].set_value('5000')
        self.op.options['crop_border'].set_value('4')

    def train(self):
        model = ConvNet(self.op, self.load_dic)
        model.start()
