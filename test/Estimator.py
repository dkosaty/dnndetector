class Estimator:

    def __init__(self):
        self.true_positive = 0
        self.false_positive = 0
        self.true_negative = 0
        self.false_negative = 0

        self.precision = 0
        self.recall = 0
        self.accuracy = 0

    def estimate(self, real_labels, predicted_labels):
        validation_data_size = len(real_labels)

        print 'validation data size =', validation_data_size

        # for i in range(N):
        #     print i+1, real_labels[i], predicted_labels[i]

        for i in range(validation_data_size):
            if real_labels[i] == predicted_labels[i]:
                if predicted_labels[i] == 1:
                    self.true_positive += 1
                else:
                    self.true_negative += 1
            else:
                if predicted_labels[i] == 1:
                    self.false_positive += 1
                else:
                    self.false_negative += 1

        tp = self.true_positive
        fp = self.false_positive
        tn = self.true_negative
        fn = self.false_negative

        self.precision = tp / float(tp + fp) * 100
        self.recall = tp / float(tp + fn) * 100
        self.accuracy = (tp + tn) / float(tp + tn + fp + fn) * 100

    def __str__(self):
        system_estimates = 'TP = %d,  FP = %d, TN = %d, FN = %d' % \
                         (self.true_positive, self.false_positive, self.true_negative, self.false_negative)

        classifier_estimates = 'precision = %f, recall = %f, accuracy = %f' % \
                               (self.precision, self.recall, self.accuracy)

        return str(system_estimates) + '\n' + str(classifier_estimates)