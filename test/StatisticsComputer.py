import os
import re
import cPickle
from ConfigParser import SafeConfigParser
import numpy as np

from convnet import IGPUModel
from shownet import ShowConvNet

from Estimator import *


class StatisticsComputer:

    def __init__(self, config_file):
        config_parser = SafeConfigParser()

        # parse the configuration file
        config_parser.read(config_file)

        self.op = ShowConvNet.get_options_parser()

        save_path = config_parser.get('save-path', 'save-path')
        self.load_dic = self.load_checkpoint(save_path)

        self.op.options['test_batch_range'].set_value(config_parser.get('range', 'test-range'))
        self.op.options['feature_path'].set_value(config_parser.get('feature-path', 'feature-path'))
        self.op.options['write_features'].set_value("probs")

    def load_checkpoint(self, save_path):
        load_dic = None

        absolute_path = os.path.abspath(save_path)
        checkpoint_path = StatisticsComputer.get_checkpoint_path(absolute_path)

        if checkpoint_path:
            self.op.options['load_file'].set_value(checkpoint_path)

            load_dic = IGPUModel.load_checkpoint(self.op.options['load_file'].value)
            old_op = load_dic['op']
            old_op.merge_from(self.op)
            self.op = old_op

        return load_dic

    @staticmethod
    def get_checkpoint_path(save_path):
        checkpoint_path = None

        if os.path.exists(save_path):
            convnet_folder = str(filter(lambda x: x.startswith('ConvNet'), os.listdir(save_path)).pop())
            if convnet_folder:
                convnet_path = os.path.join(save_path, convnet_folder)
                if os.path.exists(convnet_path):
                    checkpoint_name = str(os.listdir(convnet_path).pop())
                    if checkpoint_name:
                        checkpoint_path = os.path.join(save_path, convnet_folder, checkpoint_name)

        return checkpoint_path

    @staticmethod
    def parse_range(scope):
        pattern = r'(\d+)-?(\d+)?'

        founded = re.match(pattern, scope)

        if founded.group(2) is None:
            first = int(founded.group(1))
            last = first
        else:
            first, last = int(founded.group(1)), int(founded.group(2))

        batches_names = ['data_batch_'+str(num) for num in range(first, last+1)]

        return batches_names, last-first

    def compute(self):
        model = ShowConvNet(self.op, self.load_dic)
        model.do_write_features()

        scale = str(self.op.get_value('test_batch_range').pop())
        batches_list, batches_number = StatisticsComputer.parse_range(scale)

        real_labels, predicted_labels = [], []

        for batch in batches_list:
            data = cPickle.load(open(os.path.join(self.op.get_value('feature_path'), batch), 'rb'))

            label = np.argmax(data['data'], axis=1)
            predicted_labels += label.tolist()

            label = np.asarray(data['labels'], dtype=np.int)
            real_labels += label[0].tolist()

        estimator = Estimator()
        estimator.estimate(real_labels=real_labels, predicted_labels=predicted_labels)

        return estimator
