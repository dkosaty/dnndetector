# environment variables:
# LD_LIBRARY_PATH = /usr/local/cuda-7.5/lib64
# PYTHONPATH = /home/dmitry/cuda_convnet
#
# script parameters:
# -c params-test/*-test.cfg

from optparse import OptionParser
from StatisticsComputer import *


def main():
    option_parser = OptionParser()

    option_parser.add_option('-c', '--configuration_file', dest='config_file', type='string')

    (options, args) = option_parser.parse_args()

    statistics_computer = StatisticsComputer(options.config_file)

    estimates = statistics_computer.compute()

    print estimates

if __name__ == '__main__':
    main()
